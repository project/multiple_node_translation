<?php
namespace Drupal\multiple_node_translation;

use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\Language\LanguageManager;
use Drupal\node\Entity\Node;
use Drupal\pathauto\PathautoGenerator;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Messenger\MessengerTrait;
class CreateNodes {
  use DependencySerializationTrait;
  use MessengerTrait;
  /**
   * @var LanguageManager
   */
  protected $language_manager;

  /**
   * @var PathautoGenerator
   */
  protected $pathauto_generator;

  /**
   * @var EntityRepository
   */
  protected $entity_repository;


  /**
   * CreateNodes constructor.
   * @param LanguageManager $language_manager
   * @param PathautoGenerator $pathauto_generator
   * @param EntityRepository $entity_repository
   */
  public function __construct(LanguageManager $language_manager, PathautoGenerator $pathauto_generator, EntityRepository $entity_repository) {
    $this->pathauto_generator = $pathauto_generator;
    $this->language_manager = $language_manager;
    $this->entity_repository = $entity_repository;
  }


  public function processNodes($values, &$context){
    $languages = $this->language_manager->getStandardLanguageList();

    $language_name = $languages[$values[1]][0];
    $message = 'Creating Translated ' . $language_name . ' Nodes...';

    $langcode = $values[1];
    $results = array();
      $node = Node::load($values[0]);

      $existing_translation = $this->entity_repository->getTranslationFromContext($node, $langcode);

      $has_translation = ($existing_translation->langcode->value === $langcode) ? TRUE : FALSE;

      if (!$has_translation) {
        $node_trans = $node->addTranslation($langcode); // sample using Australian English
        $node_trans->setTitle($node->getTitle());
        $node_trans_fields = $node->getTranslatableFields();
        foreach ($node_trans_fields as $name => $field) {
          if (substr($name, 0, 6) == 'field_' || in_array($name, ['body', 'path', 'title'])) {
            if ($name == 'path') {
              $node_trans->set("path", ["pathauto" => TRUE]);
            } else {
              $node_trans->set($name, $field->getValue());
            }
          }
        }
        $this->pathauto_generator->updateEntityAlias($node_trans, 'update');

        $results[] = $node->save();


      }

    $context['message'] = $message;
    $context['results'] = $results;
  }

  public function createNodesFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'Success', 'Success'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    $this->messenger()->addMessage($message);
  }

}
