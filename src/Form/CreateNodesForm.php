<?php
namespace Drupal\multiple_node_translation\Form;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\multiple_node_translation\CreateNodes;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
/**
 * Class CreateNodesForm.
 *
 * @package Drupal\multiple_node_translation\Form
 */
class CreateNodesForm extends FormBase implements ContainerInjectionInterface {
  use DependencySerializationTrait;

  /**
   * @var CreateNodes
   */
  protected $multiple_node_translation;


  public static function create(ContainerInterface $container) {
    return new static(
    // Load the service required to construct this class.
      $container->get('multiple_node_translation.create_nodes')
    );
  }

  public function __construct(CreateNodes $multiple_node_translation)
  {
    $this->multiple_node_translation = $multiple_node_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'create_node_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['create_node'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Create Translated Nodes'),
    );
    $node_types = \Drupal\node\Entity\NodeType::loadMultiple();
    // If you need to display them in a drop down:
    $options = [];
    foreach ($node_types as $node_type) {
      $options[$node_type->id()] = $node_type->label();
    }
    $form['select_ct'] = array(
      '#type' => 'select',
      '#title' => $this->t('Select Content type you want create nodes for:'),
      '#options' => $options
    );

    $languages = \Drupal::languageManager()->getLanguages();
    foreach ($languages as $key => $value) {
      $curr_lang_code = \Drupal::languageManager()->getCurrentLanguage()->getId();
      if ($curr_lang_code !== $key){
        $options_lang[$key] = $key;
      }
    }


    $form['select_lang'] = array(
      '#type' => 'select',
      '#title' => $this->t('Select Language you want create nodes for:'),
      '#options' => $options_lang,
      '#default_value' => 'gsw-berne'
    );

    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $content_type = $form_state->getValue('select_ct');
    $lang_to_translate = $form_state->getValue('select_lang');

    $multiple_node_translation = $this->multiple_node_translation;

    $nids = \Drupal::entityQuery('node')
      ->condition('type', $content_type)
      ->sort('created', 'ASC')
      ->execute();

    $batch = array(
      'title' => t('Creating Translated Nodes...'),
      'operations' => [],
      'finished' => [$multiple_node_translation, 'createNodesFinishedCallback'],
      'progress_message' => t('Processed @current out of @total.'),
      'error_message'    => t('An error occurred during processing'),
    );

    foreach ($nids as $key => $value) {
      $values = [$value,$lang_to_translate];
      $batch['operations'][] = [[$multiple_node_translation, 'processNodes'], [$values]];
    }

    batch_set($batch);
  }
}
